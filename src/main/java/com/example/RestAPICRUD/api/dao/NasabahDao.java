package com.example.RestAPICRUD.api.dao;

import org.springframework.data.repository.CrudRepository;
import com.example.RestAPICRUD.api.model.Nasabah;

public interface NasabahDao extends CrudRepository<Nasabah, Integer> {

}
